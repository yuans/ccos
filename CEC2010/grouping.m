% Author: Dr. Yuan SUN
% email address: yuan.sun@unimelb.edu.au OR suiyuanpku@gmail.com
% ------------
% Description:
% ------------
% grouping - This function is used to read the RDG datafiles and
% form the subcomponents which are used by the cooperative co-evolutionary
% framework. This function is called in ccos.m.
%
%--------
% Inputs:
%--------
%    fun : the function id for which the corresponding grouping should
%          be loaded by reading the RDG datafiles.

function [allGroups] = grouping(fun)
    filename = sprintf('./rdg/results/F%02d', fun);
    load(filename);
    if isempty(seps)
        allGroups = nonseps;
    else
        allGroups=[{seps},nonseps];
    end
end
