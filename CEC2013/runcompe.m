
% set the search range, and call the decc algorithm

function [bestval] = runcompe(fname, func_num, NP, FEs, fid1, fid2)

    %for overlapping function D = 905
    if (func_num > 12 && func_num < 15)
        D = 905; % dimensionality of the objective function.
        Lbound = -100.*ones(NP,D);
        Ubound = 100.*ones(NP,D);
    elseif (func_num == 1 | func_num == 4 | func_num == 7 | func_num == 8 | func_num == 11 | func_num == 12 | func_num == 15 )
        D = 1000;
        Lbound = -100.*ones(NP,D);
        Ubound = 100.*ones(NP,D);
    elseif (func_num == 2 | func_num == 5 | func_num == 9)
        D=1000;
        Lbound = -5.*ones(NP,D);
        Ubound = 5.*ones(NP,D);
    else 
        D=1000;
        Lbound = -32.*ones(NP,D);
        Ubound = 32.*ones(NP,D);
    end
    % the main step, call decc(), see the decc.m
    [bestval]  = hcc(fname, func_num, D, NP, Lbound, Ubound, FEs, fid1, fid2);

end
