% Author: Dr. Yuan SUN
% email address: yuan.sun@unimelb.edu.au OR suiyuanpku@gmail.com
%
% ------------
% Description:
% ------------
% ccos - This function implements the CC framework that can select an appropriate 
%        optimizer from a given algorith pool for solving large-scale problems.
function [bestval] = ccos(fname,func_num,dim,NP,Lbound,Ubound,FEMax,fid1,fid2)

    % trace for the fitness value
    groupingFE = 3e6 - FEMax; % FEs used in decomposition
    
    countFE = 0;
    maxFECycle = 10000;
  
    % grouping cell
    allGroups = grouping(func_num);
    numGroups = size(allGroups, 2);
    
    % initialization for sansde 
    popsize = NP;
    ccm = 0.5*ones(1,numGroups);
    pop = Lbound + rand(popsize, dim) .* (Ubound-Lbound);
    val = feval(fname, pop,  func_num);
    [bestval, ibest] = min(val);
    xbest = pop(ibest, :);
    countFE=countFE+popsize;
    fprintf(fid1, '%d, %e\n', groupingFE, bestval);
    
    % Initialization for slpso
    v = zeros(NP, dim);
    
    % Initialization of fitness improvement array
    fitImpAccum = zeros(1,2*numGroups);
    fitness = repmat(val', [1 numGroups]);
     
    cycle = 0;
    display = 1; % 1 display results; 0 not display results 
    while (countFE < FEMax)
        cycle = cycle + 1;   
        maxVal = max(fitImpAccum);
        idx    = find(fitImpAccum == maxVal);  
        for i = 1:length(idx)    
            groupIdx = mod(idx(i)-1,numGroups)+1;
            algIdx   = ceil(idx(i)/numGroups);
            assert(algIdx==1 || algIdx==2);     
            dimIdx = allGroups{groupIdx};
            if algIdx == 1 
                [pop(:,dimIdx),fitness(:,groupIdx),xbestnew,bestvalnew,ccm(groupIdx)] = sansde(fname,func_num,dimIdx,pop(:,dimIdx),fitness(:,groupIdx),xbest,bestval,Lbound(:,dimIdx),Ubound(:,dimIdx),maxFECycle,ccm(groupIdx));               
            else
                [pop(:,dimIdx),fitness(:,groupIdx),xbestnew,bestvalnew,v(:,dimIdx)] = slpso(fname,func_num,dimIdx,pop(:,dimIdx),fitness(:,groupIdx),xbest,bestval,Lbound(:,dimIdx),Ubound(:,dimIdx),maxFECycle,v(:,dimIdx));
            end          
            if bestvalnew < bestval
                fitimp = (bestval-bestvalnew)/bestval;
                fitImpAccum(idx(i)) = (fitImpAccum(idx(i))+fitimp)/2;   
                xbest = xbestnew;
                bestval = bestvalnew;
            else
                fitImpAccum(idx(i)) = fitImpAccum(idx(i))/2;
            end
            countFE = countFE + maxFECycle; 
            fprintf(fid2, '%d, %d, %d\n', cycle, groupIdx, algIdx);
        end
        
        if(display == 1 && mod(cycle,1)==0)
           fprintf(1, 'Cycle = %d, bestval = %e, component = %d, algorithm = %d, \n', cycle, bestval, groupIdx, algIdx);
           fprintf(fid1, '%d, %e\n', countFE + groupingFE, bestval);
        end
    end
    fprintf(1, 'Cycle = %d, bestval = %e, component = %d, algorithm = %d, \n', cycle, bestval, groupIdx, algIdx);
end
                
